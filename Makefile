FIAM_DS_IMAGE_NAME=sidri/ldap
FIAM_DS_IMAGE_VERSION=2.5.12

build-fiam-ds-b0:
	docker build --no-cache -t sidri/ldap:b0 -f fiam-directory-server-build-0 .
build-fiam-ds-b1:
	docker build -t sidri/ldap:b1 -f fiam-directory-server-build-1 .
build-fiam-ds:
	docker build -t sidri/ldap:2.5.12 -f fiam-directory-server-2.5.12 .
run-fiam-ds:
	docker run --rm -d -p 389:389 $(FIAM_DS_IMAGE_NAME):$(FIAM_DS_IMAGE_VERSION)
push-fiam-ds:
	docker push $(FIAM_DS_IMAGE_NAME):$(FIAM_DS_IMAGE_VERSION)

CDN_DOCKERFILE=content-delivery-1.0.0
CDN_IMAGE_NAME=sidri/cdn
CDN_IMAGE_VERSION=1.0.0
build-cdn:
	docker build -t sidri/cdn:1.0.0 -f $(CDN_DOCKERFILE) .
push-cdn:
	docker push $(CDN_IMAGE_NAME):$(CDN_IMAGE_VERSION)
